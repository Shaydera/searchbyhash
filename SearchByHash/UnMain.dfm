object FmMain: TFmMain
  Left = 0
  Top = 0
  Caption = 'Search By Hash'
  ClientHeight = 312
  ClientWidth = 371
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object pnlMain: TPanel
    Left = 0
    Top = 0
    Width = 371
    Height = 312
    Align = alClient
    TabOrder = 0
    DesignSize = (
      371
      312)
    object edSearchPath: TLabeledEdit
      AlignWithMargins = True
      Left = 16
      Top = 32
      Width = 285
      Height = 21
      Anchors = [akLeft, akTop, akRight]
      EditLabel.Width = 44
      EditLabel.Height = 13
      EditLabel.Caption = 'Directory'
      TabOrder = 0
    end
    object btnSearchPathOpen: TBitBtn
      Left = 307
      Top = 30
      Width = 45
      Height = 25
      Anchors = [akTop, akRight]
      Caption = '...'
      TabOrder = 1
      OnClick = btnSearchPathOpenClick
    end
    object edHashInput: TLabeledEdit
      Left = 16
      Top = 75
      Width = 209
      Height = 21
      Anchors = [akLeft, akTop, akRight]
      EditLabel.Width = 28
      EditLabel.Height = 13
      EditLabel.Caption = 'Hash:'
      TabOrder = 2
    end
    object btnSearch: TBitBtn
      Left = 16
      Top = 102
      Width = 336
      Height = 25
      Anchors = [akLeft, akTop, akRight]
      Caption = 'Search'
      TabOrder = 3
      OnClick = btnSearchClick
    end
    object cbbHashType: TComboBox
      AlignWithMargins = True
      Left = 282
      Top = 75
      Width = 70
      Height = 21
      Anchors = [akTop, akRight]
      TabOrder = 4
      Text = 'cbbHashType'
    end
    object lbResults: TListBox
      Left = 16
      Top = 133
      Width = 336
      Height = 164
      Anchors = [akLeft, akTop, akRight, akBottom]
      ItemHeight = 13
      TabOrder = 5
    end
    object edExtension: TLabeledEdit
      Left = 231
      Top = 75
      Width = 45
      Height = 21
      Anchors = [akLeft, akTop, akRight]
      EditLabel.Width = 24
      EditLabel.Height = 13
      EditLabel.Caption = 'Mask'
      TabOrder = 6
      Text = '*.*'
    end
  end
end
