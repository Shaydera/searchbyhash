unit UnMain;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.ExtCtrls, Vcl.StdCtrls, Vcl.Buttons,
  Vcl.FileCtrl, Vcl.ComCtrls, RTTI, System.Generics.Collections, UnHelper, System.IOUtils;

type
  TFmMain = class(TForm)
    pnlMain: TPanel;
    edSearchPath: TLabeledEdit;
    btnSearchPathOpen: TBitBtn;
    edHashInput: TLabeledEdit;
    btnSearch: TBitBtn;
    cbbHashType: TComboBox;
    lbResults: TListBox;
    edExtension: TLabeledEdit;
    procedure btnSearchPathOpenClick(Sender: TObject);
    procedure btnSearchClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
  public
  end;

var
  FmMain: TFmMain;

implementation

{$R *.dfm}

procedure TFmMain.btnSearchClick(Sender: TObject);
var
  vFile: string;
  vTmpHash: string;
  vHashOption: THashTypes;
  vNeedle: string;
begin
  if not DirectoryExists(edSearchPath.Text) then
    Exit;

  vNeedle := UpperCase(edHashInput.Text);
  lbResults.Items.Clear();
  vHashOption := THashTypes(cbbHashType.ItemIndex);
  for vFile in TDirectory.GetFiles(edSearchPath.Text, edExtension.Text, TSearchOption.soAllDirectories) do
  begin
    vTmpHash := THelperUtilities.GetFileHash(vFile, vHashOption);
    if vTmpHash.Equals(vNeedle) then
      lbResults.Items.Add(vFile);
  end;
end;

procedure TFmMain.btnSearchPathOpenClick(Sender: TObject);
var
  lDir: string;
  lSuccess: Boolean;
begin
  lDir := edSearchPath.Text;
  with TFileOpenDialog.Create(nil) do
  try
    OkButtonLabel := 'OK';
    Options := [fdoPickFolders, fdoPathMustExist, fdoNoReadOnlyReturn];
    if System.SysUtils.DirectoryExists(lDir) then
      DefaultFolder := lDir;
    lSuccess := Execute();
    if lSuccess Then
      lDir := FileName;
  finally
    Free;
  end;
  edSearchPath.Text := lDir;
end;

procedure TFmMain.FormCreate(Sender: TObject);
var
  hashType: THashTypes;
begin
  cbbHashType.Clear;
  for hashType := Low(THashTypes) to High(THashTypes) do
  begin
    cbbHashType.Items.Add(TRttiEnumerationType.GetName(hashType));
  end;
  cbbHashType.ItemIndex := Ord(Low(THashTypes));
end;

end.
