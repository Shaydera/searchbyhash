program SearchByHash;

uses
  Vcl.Forms,
  UnMain in 'UnMain.pas' {FmMain},
  UnHelper in 'UnHelper.pas',
  Vcl.Themes,
  Vcl.Styles;

{$R *.res}

begin
  Application.Initialize;
  Application.MainFormOnTaskbar := True;
  TStyleManager.TrySetStyle('Material');
  Application.CreateForm(TFmMain, FmMain);
  Application.Run;
end.
