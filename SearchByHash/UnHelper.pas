unit UnHelper;

interface

uses
  System.SysUtils, Winapi.Windows, System.Hash, System.IOUtils;

type
  THashTypes = (MD5, SHA1);

  THelperUtilities = class(TObject)
    strict private
      const TwoDigitLookup : packed array[0..99] of array[1..2] of Char =
                    ('00','01','02','03','04','05','06','07','08','09',
                     '10','11','12','13','14','15','16','17','18','19',
                     '20','21','22','23','24','25','26','27','28','29',
                     '30','31','32','33','34','35','36','37','38','39',
                     '40','41','42','43','44','45','46','47','48','49',
                     '50','51','52','53','54','55','56','57','58','59',
                     '60','61','62','63','64','65','66','67','68','69',
                     '70','71','72','73','74','75','76','77','78','79',
                     '80','81','82','83','84','85','86','87','88','89',
                     '90','91','92','93','94','95','96','97','98','99');
    protected
      class function IntToStr32(pValue: Cardinal; pNegative: Boolean): string;
    public
      class function iToA(pValue: Cardinal): string;
      class function GetFileHash(pFilePath: string; pHashType: THashTypes): string;
  end;
implementation

class function THelperUtilities.GetFileHash(pFilePath: string; pHashType: THashTypes): string;
var
  vHashMd5: THashMD5;
  vHashSha1: THashSHA1;
  vFileBytes: TBytes;
begin
  Result := '';

  try
    vFileBytes := TFile.ReadAllBytes(pFilePath);
  except
    Result := 'FileError';
    Exit();
  end;

  case pHashType of
    MD5:
      begin
        vHashMd5.Reset();
        vHashMd5.Update(vFileBytes);
        Result := UpperCase(vHashMd5.HashAsString);
      end;
    SHA1:
      begin
        vHashSha1.Reset();
        vHashSha1.Update(vFileBytes);
        Result := UpperCase(vHashSha1.HashAsString);
      end;
  end;
end;

class function THelperUtilities.IntToStr32(pValue: Cardinal; pNegative: Boolean): string;
var
  I, J, K : Cardinal;
  Digits  : Integer;
  P       : PChar;
  NewLen  : Integer;
begin
  I := pValue;
  if I >= 10000 then
    if I >= 1000000 then
      if I >= 100000000 then
        Digits := 9 + Ord(I >= 1000000000)
      else
        Digits := 7 + Ord(I >= 10000000)
    else
      Digits := 5 + Ord(I >= 100000)
  else
    if I >= 100 then
      Digits := 3 + Ord(I >= 1000)
    else
      Digits := 1 + Ord(I >= 10);
  NewLen  := Digits + Ord(pNegative);
  SetLength(Result, NewLen);
  P := PChar(Result);
  P^ := '-';
  Inc(P, Ord(pNegative));
  if Digits > 2 then
    repeat
      J  := I div 100;
      K  := J * 100;
      K  := I - K;
      I  := J;
      Dec(Digits, 2);
      PDWord(P + Digits)^ := DWord(TwoDigitLookup[K]);
    until Digits <= 2;
  if Digits = 2 then
    PDWord(P+ Digits-2)^ := DWord(TwoDigitLookup[I])
  else
    PChar(P)^ := Char(I or ord('0'));
end;

class function THelperUtilities.iToA(pValue: Cardinal): string;
begin
  Result := IntToStr32(pValue, not (pValue > 0));
end;

end.
